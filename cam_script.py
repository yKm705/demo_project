#!/usr/bin/python3
import picamera
import time

currentTime = time.asctime( time.localtime(time.time()))
path = '/home/pi/demo_project/public/images/'

with picamera.PiCamera() as camera:
    # camera.resolution = (800, 600)
    time.sleep(2)
    camera.capture(path + str(currentTime) + ".jpg")
