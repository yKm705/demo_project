# What this is:
This is the starting point on a project I had been considering for awhile. The project aims to create a server which
(currently) is hosted on a RaspberryPi 3 with a camera attachment. It is intended to be a system allowing capturing of 
wildlife. Next step is incorporating a motion sensor.

# Requirements:
* 'Package.json' contains the majority of the requirements.
* The camera capturing is controlled by a Python3 script and requires the Picamera module.