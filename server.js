const express = require('express');
// const livereload = require('express-livereload');
const path = require('path');
const fs = require('fs');
// const compression = require('compression');
const app = express();

var exec = require('child_process').exec, child;

// Allows for automatic file reloading.
// livereload(app, config={});

// app.use(compression());
app.use(express.static(path.join(__dirname, 'public')));

// GET request to retrieve to images stored in the 'images' directory.
app.get('/getImages', function(req, res){
    var p = path.join(__dirname, 'public/images');
    fs.readdir(p, function (err, items) {
        if(err){
            console.err("Error: " + err);
            res.end();
        }
        items.sort(function(a, b) {
                return fs.statSync(path.join(p, a)).birthtime.getTime() -
                    fs.statSync(path.join(p, b)).birthtime.getTime()
            });
        items.reverse();
        res.send(items);
    });
});

// GET request at this URL runs the image capture program on the raspberrypi.
app.get('/capture', function (req, res) {
    var returnJson = {
        'stdout' : null,
        'stderr' : null
    };
    // To take images remotely.
    child = exec('./cam_script.py', function (error, stdout, stderr) {
        returnJson['stdout'] = stdout;
        returnJson['stderr'] = stderr;
        res.send(returnJson);
    });
});

// AngularJS and NodeJS.
app.use('*', function (req, res) {
    res.sendFile(__dirname + '/public/index.html');
});

// Listening to requires.
const server = app.listen(3000, function() {
    const port = server.address().port;
    // console.log('Example app listen @ port 3000!')
    console.log('Example app listen @ port ' + port);
});
