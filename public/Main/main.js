(function () {

    angular.module('myApp.main', ['ngRoute', 'ui.bootstrap'])
        .controller('mainController', mainController)
        .directive('imageDisplay', imageDisplay);

    // Allow minifying.
    mainController.$inject = ['$http', '$uibModal'];

    function mainController($http, $uibModal) {

        var mc = this;

        // Functions
        mc.getImages = getImages;
        mc.getImagePath = getImagePath;
        mc.capture = capture;
        mc.open = open;

        // Variables.
        mc.programOut = {
            'stdout': null,
            'stderr': null,
            'error': null
        };
        mc.images = [];
        mc.errorStatus = "";
        mc.takingImage = false;
        mc.loadingImages = false;

        // Initialisation Point for the controller.
        init();

        ////////////////

        function init(){
            getImages();
        }

        function getImages() {
            // $('#takingImageSpinner').show();
            mc.loadingImages = true;
            $http({
                method: 'GET',
                url: '/getImages'
            }).then(function (response) {
                mc.images = response.data;
                mc.loadingImages = false;

            }, function (response) {
                mc.loadingImages = false;
            });
        }

        function getImagePath(image){

            return "/images/" + image;
        }

        function capture(){
            mc.takingImage = true;

            // Clean messages.
            mc.captureError = "";
            mc.programOut['stdout'] = "";
            mc.programOut['stderr'] = "";
            $http({
                method: 'GET',
                url: '/capture'
            }).then(function (response) {
                // $("#captureButton").attr("disable", false);
                mc.programOut['stdout'] = response.data['stdout'];
                mc.programOut['stderr'] = response.data['stderr'];

                // Setting error message statement.
                if(mc.programOut['stderr'] !== ""){
                    mc.captureError = true;
                }
                else{
                    mc.captureError = false;
                }

                mc.takingImage = false;
                // Reload images.
                getImages();

                // Error
            }, function (response) {

                mc.takingImage = false;

                mc.programOut['stdout'] = response.data['stdout'];
                mc.programOut['stderr'] = response.data['stderr'];
                console.err("Failed: " + response.data);
            });
        }

        function open(image){
           var modalInstance = $uibModal.open({
               animation: true,
               ariaLabelledBy: 'modal-title',
               ariaDescribedBy: 'modal-body',
               size: 'lg',
               backdrop: true,
               templateUrl: "modalContent.html",
               controller: function($scope, $uibModalInstance){
                   $scope.image = image;
                   $scope.path = mc.getImagePath(image);
                   $scope.images = mc.images;

                   $scope.index = $scope.images.indexOf(image);

                   $scope.previous = function(){

                       if($scope.index !== 0){
                           $scope.image = $scope.images[$scope.index - 1];
                           $scope.path = mc.getImagePath($scope.image);
                           $scope.index = $scope.images.indexOf($scope.image);
                       }
                   };

                   $scope.next = function(){
                       if($scope.index !== $scope.images.length - 1){
                           $scope.image = $scope.images[$scope.index + 1];
                           $scope.path = mc.getImagePath($scope.image);
                           $scope.index = $scope.images.indexOf($scope.image);
                       }
                   };

                   $scope.ok = function(){
                       $uibModalInstance.close();
                   };
               }
           });

           // To catch exception when existing modal.
           modalInstance.result.then(function(){
            }, function(){
           });
        }
    }

    function imageDisplay() {
        return {
            scope: {
                image: '=',
                path: '=',
                open: '='
            },
            replace: true,
            restrict: 'E',
            templateUrl: "Main/partials/imageDisplay.html"
        };
    }
})();

