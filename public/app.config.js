var app = angular.module('myApp', [
    'myApp.welcome',
    'myApp.main'
    ]);
app.config(function ($routeProvider) {
    // Welcome section.
    $routeProvider
        .when('/', {
            templateUrl: 'Welcome/welcome.html',
            controller: 'welcomeController'
        })
        .when('/main', {
            templateUrl: 'Main/main.html',
            controller: 'mainController',
            controllerAs: 'mc'
        })
        .otherwise({
            redirectTo: '/'
        });
});
